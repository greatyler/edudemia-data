<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_data-types',
		'title' => 'Data Types',
		'fields' => array (
			array (
				'key' => 'field_5714f1cc4d15e',
				'label' => 'Singular Name',
				'name' => 'singular_name',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'datatype',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}













?>
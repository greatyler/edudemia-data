<?php


function edudms_data_maker(){
// WP_Query arguments
$args = array (
	'post_type'              => 'datatype',
	'post_status'            => 'publish',
);

// The Query
$data_types_query = new WP_Query( $args );
$data_type_posts = $data_types_query->get_posts();

foreach ( $data_type_posts as $dt ) {
		$id = $dt->ID;
		$name = $dt->post_title;
		$slug = name_slugger($name);
		$sing_name = get_post_meta($id, 'singular_name', true);
		
		//$supports_array = create_supports_array($id);
		
		
	// Register Custom Post Type

	$labels = array(
		'name'                  => _x( $name, 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( $sing_name, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( $name, 'text_domain' ),
		'name_admin_bar'        => __( $name, 'text_domain' ),
		'archives'              => __( $sing_name.' Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent '.$sing_name.':', 'text_domain' ),
		'all_items'             => __( 'All '.$name, 'text_domain' ),
		'add_new_item'          => __( 'Add New '.$sing_name, 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New '.$sing_name, 'text_domain' ),
		'edit_item'             => __( 'Edit '.$sing_name, 'text_domain' ),
		'update_item'           => __( 'Update '.$sing_name, 'text_domain' ),
		'view_item'             => __( 'View '.$sing_name, 'text_domain' ),
		'search_items'          => __( 'Search '.$sing_name, 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into '.$sing_name, 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this '.$sing_name, 'text_domain' ),
		'items_list'            => __( $name.' list', 'text_domain' ),
		'items_list_navigation' => __( $name.' list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter '.$name.' list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Test', 'text_domain' ),
		'description'           => __( 'This is the Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'thumbnail', 'revisions', 'author'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	
	register_post_type( $slug, $args );
}


}

		
		

	
add_action( 'init', 'edudms_data_maker' );	
	
	





//add_shortcode('test_maker', 'test_supports_array');
/*
function test_supports_array() {
	$supports_array = create_supports_array('89');
	print_r($supports_array);
	echo $supports_array;
	
	
}


//creates the supports array from the various custom fields.
function create_supports_array($post_id) {

	//$supports_array = array();
	//$supports_array = get_field('supports');
	$supports_array = array( 'title', 'editor', 'thumbnail');
	
	
return $supports_array;

}
*/

function name_slugger($name) {
	
	$lower_name = strtolower($name);
	$clean_name = preg_replace('/\s/', '', $lower_name);
	
	return $clean_name;
	//return $lower_name;
}

?>
<?php
/*
Plugin Name: Edudemia Data Addon
Plugin URI: 
Description: Add data management to your DMS
Version: 0.0.1
Author: Tyler Pruitt
Author URI:
Bitbucket Plugin URI: greatyler/edudemia-data
Bitbucket Branch: realmaster

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:





License

Edudemia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Edudemia software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/

include(dirname( __FILE__ ) . '/data_menu_page.php');
include(dirname( __FILE__ ) . '/custom_fields_datatype.php');
include(dirname( __FILE__ ) . '/maker.php');
include(dirname( __FILE__ ) . '/shortcode.php');



//Activation hooks
register_activation_hook( __FILE__, 'edudms_data_activate' );

function edudms_data_activate() {
	
}


function edudms_data_submenu_settings() {
add_submenu_page('options-general.php', 'Data Settings', 'EduDMS Data', 'manage_options', 'event_settings', 'edudms_data_menu_render');
}
add_action("admin_menu", 'edudms_data_submenu_settings');



// Function to create custom post type: datatype
function edudms_data_create_datatype_posttype() {

	register_post_type( 'datatype',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Data Types' ),
				'singular_name' => __( 'Data Type' )
			),
			'public' => true,
			'has_archive' => true,
			'description' => 'Data Types for use by edudms Data Addon',
			'rewrite' => array('slug' => 'datatype'),
			'supports' => array( 'title', 'editor', 'thumbnail',  )
		)
	);
}
// Hooking up to theme setup
add_action( 'init', 'edudms_data_create_datatype_posttype' );

	






















?>